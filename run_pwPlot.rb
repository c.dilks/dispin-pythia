#!/usr/bin/env ruby
# run pwPlot.py for the specified schemes

# settings ############################################################
runName  = "run3"               # output files will start with this name
schemes  = [2,3]                # plot schemes (see pwPlot.py)
bruDir   = "bruspin/#{runName}" # assume output files are #{bruDir}.#{binName} (see below)
binnings = [                    # binning types (cf. run_pwFit.rb)
  { :names=>[:z]    },
  { :names=>[:m]    },
  { :names=>[:z,:m] },
]
#######################################################################

# latex titles
xTitles = {
  :x  => '$x$',
  :m  => '$M_h$',
  :z  => '$z$',
  :pt => '$p_T$',
  :q  => '$Q^2$',
  :xf => '$x_F$',
}

# execution
resDirList = []
binnings.product(schemes).each do |binning,scheme|
  binName = binning[:names].map(&:to_s).join('-')
  resDir = "#{bruDir}.#{binName}"
  resDirList << resDir
  resFiles = Dir.glob("#{resDir}/asym*.root").sort
  args = [
    "-s #{scheme}",
    "-x '#{xTitles[binning[:names].first]}'",
    "-o png",
    *resFiles,
  ]
  system "./pwPlot.py #{args.join ' '}"
end
resDirList.uniq!

# list images
imgList = resDirList.map do |resDir|
  Dir.glob("#{resDir}/*.png").sort
end.flatten
puts "\nproduced images:\n"+"="*30
puts imgList
puts "\none line:\n#{imgList.join(' ')}"
