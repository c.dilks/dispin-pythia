include config.mk

# dispin/src dependencies
DEPS += -Isrc
# LIBS += -L. -l$(DISPIN)
LIBS += $(STRINGSPINNER)/mc3P0.o -lgfortran

# assume each .cpp file has main and build corresponding .exe executable
SOURCES := $(basename $(wildcard *.cpp))
EXES := $(addsuffix .exe, $(SOURCES))

all: 
	buildBrufit.sh
	envStringSpinner.sh $(PYTHIADIR)
	@cd stringspinner; make dis
	envStringSpinnerRevert.sh
	@cd src; make
	make exe

exe: $(EXES)

%.exe: %.o
	@echo "--- make executable $@"
	$(CXX) -o $@ $< ./$(DISPINOBJ) $(LIBS)

%.o: %.cpp
	@echo "----- build $@ -----"
	$(CXX) -c $^ -o $@ $(FLAGS) $(DEPS)

clean:
	@cd stringspinner; make clean
	@cd src; make clean
	rm -r brufit/build
	$(RM) $(EXES)
