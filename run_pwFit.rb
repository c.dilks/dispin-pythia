#!/usr/bin/env ruby
# run pwFit.py for the specified binnings

require 'awesome_print'

# settings ############################################################
runName   = "run3"                     # output files will start with this name
inFile    = "out/#{runName}.hadd.root" # input ROOT tree
bruDir    = 'bruspin'                  # output files will be in #{resDir} (see below)
minimizer = 'minuit'                   # which optimizer to use for the maximum likelihood determination
# binning types (see #{ivTypeHash} keys below for available names)
# - each scheme in the list will be fitted for
binnings = [
  { :names=>[:z],    :bins=>[6]   }, # 6 bins in z
  { :names=>[:m],    :bins=>[6]   }, # 6 bins in M_h
  { :names=>[:z,:m], :bins=>[3,2] }, # 3 bins in z, for 2 bins in M_h
]
#######################################################################

# convert binning[:names] to ivType number (see src/Binning.*, or run `new Binning()`)
def getIVtype(namesList)
  ivTypeHash = {
    :x  => 1,
    :m  => 2,
    :z  => 3,
    :pt => 4,
    :q  => 5,
    :xf => 6,
  }
  namesList.map{ |name| ivTypeHash[name] }.join.to_i
end

# execution
binnings.each do |binning|

  # set output directory
  binName = binning[:names].map(&:to_s).join('-')
  resDir = "#{bruDir}/#{runName}.#{binName}"

  # pwFit.C arguments
  args = [
    inFile,
    "",
    resDir,
    minimizer,
    "",
    getIVtype(binning[:names]),
    *binning[:bins],
  ]
  args.map!{ |arg| if arg.class==String then "\"#{arg}\"" else arg end } # add quotes around strings
  ap args

  # pwFit.C execution
  brufit = "root -b -q $BRUFIT/macros/LoadBru.C"
  system "#{brufit} 'pwFit.C(#{args.join ','})'"
end
