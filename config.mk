# compiler and flags
CXX = g++
FLAGS = -g -Wno-deprecated -fPIC -fno-inline -Wno-write-strings

# extra flags for valgrind
FLAGS += -O0

# ROOT
DEPS = -I$(shell root-config --incdir)
LIBS = $(shell root-config --glibs)
LIBS += -lMinuit -lRooFitCore -lRooFit -lRooStats -lProof -lMathMore

# PYTHIA
# env var PYTHIADIR must be set (e.g., `source env.sh`)
DEPS += -I$(PYTHIADIR)/include
LIBS += -L$(PYTHIADIR)/lib -lpythia8 -ldl

# StringSpinner
# env var STRINGSPINNER must be set (e.g., `source env.sh`)
DEPS += -I$(STRINGSPINNER)
# LIBS += -L$(STRINGSPINNER)

# BruFit
DEPS += -I$(BRUFIT)/core
LIBS += -L$(BRUFIT)/lib -lbrufit

# DiSpin shared object name and source directory
DISPIN = DiSpin
DISPINOBJ := lib$(DISPIN).so
