#!/bin/bash

export PYTHIADIR=/home/dilks/b/pythia8

export STRINGSPINNER=$(pwd -P)/stringspinner

export BRUFIT=$(pwd -P)/brufit
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${BRUFIT}/lib
