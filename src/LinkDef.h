#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Tools+;
#pragma link C++ class Dihadron+;
#pragma link C++ class Binning+;
#pragma link C++ class Modulation+;
#pragma link C++ class BruAsymmetry+;
#pragma link C++ class BruBin+;
#pragma link C++ class BruTree+;

#endif
