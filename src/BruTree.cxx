#include "BruTree.h"

ClassImp(BruTree)

// constructor
BruTree::BruTree(TString outFileN_) : outFileN(outFileN_) {

  // dihadron PIDs
  dihadronPIDs[0] =  211;
  dihadronPIDs[1] = -211;

  // Dihadron object
  dih           = new Dihadron();
  dih->debug    = false;
  dih->useBreit = false;

  // tree
  outFile = new TFile(outFileN,"RECREATE");
  tree    = new TTree("tree","tree");

  // branches
  //// dis
  tree->Branch("Q2",       &Q2,               "Q2/D");
  tree->Branch("W",        &W,                "W/D");
  tree->Branch("X",        &x,                "X/D");
  tree->Branch("y",        &y,                "y/D");
  //// hadrons
  tree->Branch("hadIdx",   hadIdx,            "hadIdx[2]/I");
  tree->Branch("hadPid",   hadPid,            "hadPid[2]/I");
  tree->Branch("hadE",     dih->hadE,         "hadE[2]/D");
  tree->Branch("hadP",     dih->hadP,         "hadP[2]/D");
  tree->Branch("hadPt",    dih->hadPt,        "hadPt[2]/D");
  tree->Branch("hadEta",   dih->hadEta,       "hadEta[2]/D");
  tree->Branch("hadPhi",   dih->hadPhi,       "hadPhi[2]/D");
  tree->Branch("hadXF",    dih->hadXF,        "hadXF[2]/D");
  tree->Branch("hadZ",     dih->z,            "hadZ[2]/D");
  /// dihadrons
  tree->Branch("Mh",       &(dih->Mh),        "Mh/D");
  tree->Branch("Mmiss",    &(dih->Mmiss),     "Mmiss/D");
  tree->Branch("Z",        &(dih->zpair),     "Z/D");
  tree->Branch("XF",       &(dih->xF),        "XF/D");
  tree->Branch("Theta",    &(dih->theta),     "Theta/D");
  tree->Branch("Ph",       &(dih->PhMag),     "Ph/D");
  tree->Branch("PhPerp",   &(dih->PhPerpMag), "PhPerp/D");
  tree->Branch("PhiH",     &(dih->PhiH),      "PhiH/D");
  tree->Branch("PhiR",     &(dih->PhiR),      "PhiR/D");
  tree->Branch("PhiS",     &PhiS,             "PhiS/D");
  tree->Branch("Spin_idx", &Spin,             "Spin_idx/I");
  tree->Branch("Depol2",   &Depol2,           "Depol2/D");
  tree->Branch("Depol3",   &Depol3,           "Depol3/D");
};

// fill tree
void BruTree::Fill(Pythia8::Event& EV, Pythia8::DISKinematics DIS) {
  // reset
  for(int h=0; h<2; h++) hadIdxList[h].clear();
  dih->ResetVars();
  // dis
  Q2 = (Double_t) DIS.Q2;
  W  = (Double_t) TMath::Sqrt(DIS.W2);
  x  = (Double_t) DIS.xB;
  y  = (Double_t) DIS.y;
  // depolarization
  Double_t gamma = 2*PartMass(kP)*x / TMath::Sqrt(Q2);
  Double_t epsilon = ( 1 - y - TMath::Power(gamma*y,2)/4 ) /
    ( 1 - y + y*y/2 + TMath::Power(gamma*y,2)/4 );
  Depol2 = TMath::Sqrt(1-epsilon*epsilon);
  Depol3 = TMath::Sqrt(2*epsilon*(1-epsilon));
  // find hadrons
  for(int i=0; i<EV.size(); i++) {
    auto p = EV[i];
    if(p.isFinal()) {
      for(int h=0; h<2; h++) {
        if(p.id()==dihadronPIDs[h]) hadIdxList[h].push_back(i);
      }
    }
  }
  // pair hadrons, calculate dihadron kinematics, apply cuts, fill tree
  for(auto idx0 : hadIdxList[0]) {
    for(auto idx1 : hadIdxList[1]) {
      hadIdx[0] = idx0;
      hadIdx[1] = idx1;
      // pair hadrons
      for(int h=0; h<2; h++) {
        auto p = EV[hadIdx[h]];
        hadPid[h] = (Int_t) p.id();
        hadVec[h].SetPxPyPzE(
            p.px(),
            p.py(),
            p.pz(),
            p.e()
            );
      }
      // calculate dihadron kinematics
      dih->CalculateKinematics(
          hadVec[qA],
          hadVec[qB],
          &DIS
          );
      // get some electron kinematics
      auto     vecElectron = Tools::PythiaVecToRoot(DIS.lepout);
      Double_t eleP        = vecElectron.P();
      Double_t eleEta      = vecElectron.Eta();
      Double_t eleTheta    = Tools::EtaToTheta(eleEta);
      // apply cuts and fill tree
      if(
             Q2         > 1.0
          && W          > 2.0
          && y          < 0.8
          && dih->zpair < 0.95
          && dih->Mmiss > 1.5
          && eleP       > 2
          && eleTheta          > 5    && eleTheta          < 35
          && dih->hadTheta[qA] > 5    && dih->hadTheta[qA] < 35
          && dih->hadTheta[qB] > 5    && dih->hadTheta[qB] < 35
          && dih->hadP[qA]     > 1.25 && dih->hadP[qB]     > 1.25
          && dih->hadXF[qA]    > 0    && dih->hadXF[qB]    > 0
          && dih->PhiH > UNDEF
          && dih->PhiR > UNDEF
          && PhiS      > UNDEF
        ) {
        tree->Fill();
      };
    }
  }
};

// write tree and close file
void BruTree::Write() {
  outFile->cd();
  tree->Write();
  std::cout << "BruTree entries = " << tree->GetEntries() << std::endl;
  outFile->Close();
  std::cout << outFileN << " written." << std::endl;
};
