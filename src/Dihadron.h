#ifndef Dihadron_
#define Dihadron_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <map>
#include <vector>

// ROOT
#include "TSystem.h"
#include "TObject.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TMath.h"

// dispin
#include "Constants.h"
#include "Tools.h"

// stringspinner
#include "StringSpinner.h"


class Dihadron : public TObject
{
  public:
    Dihadron();
    ~Dihadron();

    Bool_t debug;
    Bool_t debugTheta;
    Bool_t useBreit;

    void CalculateKinematics(
        TLorentzVector trajA,
        TLorentzVector trajB,
        Pythia8::DISKinematics *disEv_
        );
    void ComputeAngles();
    Double_t PlaneAngle(TVector3 vA, TVector3 vB,
                       TVector3 vC, TVector3 vD);

    void ResetVars();

    Pythia8::DISKinematics *disEv;

    TLorentzVector vecHad[2]; // hadron momentum
    TLorentzVector vecPh; // dihadron total momentum
    TLorentzVector vecR; // dihadron relative momentum
    TLorentzVector vecMmiss; // used to compute missing mass

    TLorentzVector disVecBeam;
    TLorentzVector disVecTarget;
    TLorentzVector disVecElectron;
    TLorentzVector disVecW;
    TLorentzVector disVecQ;


    // BRANCHES  ///////////////////////////////////////
    //
    Double_t hadE[2]; // hadron energy
    Double_t hadP[2]; // hadron momentum
    Double_t hadPt[2]; // hadron transverse momentum
    Double_t hadEta[2]; // hadron pseudorapidity
    Double_t hadTheta[2]; // hadron scattering angle [deg]
    Double_t hadPhi[2];  // hadron lab-frame azimuth
    Double_t PhMag; // dihadron total momentum
    Double_t PhPerpMag; // transverse component of dihadron total momentum (perp frame)
    Double_t hadPperp[2]; // transverse component of hadron total momentum (perp frame)
    Double_t PhEta; // pseudorapidity of dihadron pair
    Double_t PhPhi; // azimuth of dihadron pair

    Double_t RMag; // dihadron relative momentum
    Double_t RTMag; // transverse componet of relative momentum (T-frame)
    Double_t RPerpMag; // transverse componet of relative momentum (perp-frame)

    Double_t PhiH; // angle[ reaction_plane, Ph^q ]
    Double_t z[2]; // fraction of energy of fragmenting parton
                  // carried by the hadron
    Double_t zpair; // fraction of energy of fragmenting parton
                   // carried by the hadron pair
    Double_t Mh; // dihadron invariant mass
    Double_t hadM[2]; // hadron mass
    Double_t Mmiss; // missing mass
    Double_t xF; // feynman-x
    Double_t hadXF[2]; // feynman-x for each hadron

    Double_t alpha; // dihadron opening angle
    Double_t zeta; // lab-frame energy sharing
    Double_t theta; // CoM-frame angle between Ph and P1
    Double_t thetaAlt; // alternative definiton of theta
    Double_t thetaLI; // lorentz-invariant theta

    // single-hadron PhiH
    Double_t GetSingleHadronPhiH(Int_t h_idx);


    // PhiR angle
    // defined a couple different ways since transverse components of R vary in
    // definition; there are two frames to consider (see arXiv:1707.04999):
    // -- perp-frame: "transverse" plane is normal to fragmenting
    //                quark, i.e., to q
    // -- T-frame: "transverse" plane is normal to Ph
    //

    Double_t PhiRq; // use R_perp computed via rejection w.r.t. q
    // -- COMPASS 1702.07317, but used vector rejection to get R_perp

    Double_t PhiRp; // use R_T computed via covariant kT formula
    // -- HERMES 0803.2367 angle, but used Matevosyan et al 1707.04999

    Double_t PhiRp_r; // use R_T computed via rejection w.r.t. Ph (not frame independent)
    // -- HERMES 0803.2367 angle

    Double_t PhiRp_g; // use R_T computed by projection operator "g_T", following
    // equation 9 in 1408.5721 (gliske, bacchetta, radici)

    Double_t PhiR; // preferred definition
    
    //
    ///////////////////////////////////////////////////

  private:
    int h;

    TVector3 pQ,pL,pPh,pR;
    TVector3 pHad[2];
    TVector3 pHad_Perp[2];
    TVector3 pPh_Perp;

    TVector3 pR_Perp;
    TVector3 pR_T_byKt;
    TVector3 pR_T_byRej;
    TVector3 pR_T_byProj;
    TLorentzVector vecR_T_byProj;

    Double_t xi,ratio;

    TVector3 crossAB,crossCD;
    Double_t sgn,numer,denom;

    TLorentzVector vecPh_com; // P+q COM frame Ph
    TLorentzVector vecPh_breit; // breit frame Ph
    TLorentzVector disVecQ_com; // P+q COM frame Q
    TLorentzVector vecHad_com[2]; // P+q COM frame hadron momenta
    TLorentzVector vecHad_dihCom[2]; // dihadron COM frame hadron momenta
    TVector3 pHad_com[2];
    TVector3 pHad_dihCom[2];
    TVector3 pPh_com,pPh_breit;
    TVector3 pQ_com;

    TVector3 dihComBoost, ComBoost, BreitBoost;

    Double_t MRterm[2];

  ClassDef(Dihadron,1);
};

#endif
