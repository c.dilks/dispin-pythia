#ifndef BruTree_
#define BruTree_

#include "Pythia8/Pythia.h"
#include "StringSpinner.h"

#include "Dihadron.h"

#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include <vector>

class BruTree : public TObject{

  public:

    BruTree(TString outFileN_="out.root"); // constructor
    ~BruTree(){};
    void Fill(Pythia8::Event& EV, Pythia8::DISKinematics DIS); // fill tree
    void Write(); // write tree and close file

    // setters
    void SetPhiS(Double_t val) { PhiS=Tools::AdjAngle(val); };
    void SetSpin(Int_t val) { Spin=val; };

  private:

    // file
    TString outFileN;
    TFile *outFile;
    TTree *tree;

    // branches
    Int_t hadIdx[2];
    Int_t hadPid[2];
    Double_t Q2;
    Double_t W;
    Double_t x;
    Double_t y;
    Double_t PhiS;
    Int_t Spin;
    Double_t Depol2,Depol3;

    // other
    TLorentzVector hadVec[2];
    std::vector<Int_t> hadIdxList[2];
    Int_t dihadronPIDs[2];
    Dihadron *dih;

  ClassDef(BruTree,1);
};

#endif
