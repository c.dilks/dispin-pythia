#!/usr/bin/env ruby
# run simulate.exe multi-threaded

require 'thread/pool'
require 'awesome_print'

# settings ############################################################
runName       = "run3"         # output files will start with this name
modes         = [2,3]          # modes (see simulate.cpp)
nJobsPerMode  = 12             # number of jobs, per mode
nEventsPerJob = 3e7.to_i       # number of events per job
nThreads      = `nproc`.to_i-2 # number of threads (check this)
#######################################################################

# print method
def pr(obj)
  puts "="*`tput cols`.to_i
  ap obj
end

# define arguments for each job; generates a random seed for each job
args = modes.map{ |mode|
  nJobsPerMode.times.map{ |id|
    {
      :id       => id,
      :mode     => mode,
      :seed     => rand(1..900_000_000),
      :nEvent   => nEventsPerJob,
      :outFileN => "out/#{runName}.job#{id}.mode#{mode}.root",
    }
  }
}.flatten
args.each{ |arg| arg[:logFileN] = arg[:outFileN].sub(/root$/,'log') }
pr args

# define commands
cmds = args.map{ |arg|
  [
    "simulate.exe",
    arg[:nEvent],
    arg[:mode],
    arg[:outFileN],
    arg[:seed],
    ">",
    arg[:logFileN],
  ].join(' ')
}
pr cmds

# execute
pool = Thread.pool(nThreads)
cmds.each{ |cmd|
  pool.process{
    puts "BEGIN #{cmd}"
    system cmd
    puts "END #{cmd}"
  }
}
pool.shutdown

# hadd
haddSources = Dir.glob("out/#{runName}.*.root")
  .reject{|name|name.match?(/hadd.root$/)}
system "hadd -f out/#{runName}.hadd.root #{haddSources.join(' ')}"
